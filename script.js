var clock = Vue.component('clock', {
    template: `
        <div class="clock" :style="curStyle">
            <p :style="{'font-size':fontSize}">{{value}}<p>
            <div @click="changeSelect(style.idx + 1)" 
                ref="digit" class="clock-digit" :class="{'clock-active':(value - 1)>=style.idx}" 
                v-for="style in styles" :style="style.data">
                
            </div>
        </div>
    `,
    props: {
        digits: {
            type: Number,
            default: 12
        },
        value: {
            type: Number,
            default: 1
        },
        size: {
            type: Number,
            default: 100
        }
    },
    methods: {
        calculateRadius() {
            return this.size / 2 - this.fontSize;
        },
        changeSelect(num) {
            this.$emit('input', Number(num));
        }
    },
    updated() {
        if (this.value > this.digits)
            this.changeSelect(this.digits);
    },
    computed: {
        fontSize() {
            return this.size / 5;
        },
        curStyle() {
            return {
                width: this.size,
                height: this.size,
                'border-radius': this.size + 'px'
            }
        },
        styles() {
            const radius = this.calculateRadius();
            const max = this.digits;
            return new Array(max).fill().map((el, idx) => {
                const rads = idx * (2 * Math.PI / max);
                const hord = Math.SQRT2 * radius * Math.sqrt(1 - Math.cos(rads));
                const topDistance = hord * Math.sin(rads / 2);
                const topOffset = topDistance + this.fontSize / 2;
                const leftDistance = hord * Math.cos(rads / 2);
                const leftOffset = leftDistance + radius + this.fontSize;
                return {
                    data: {
                        top: topOffset,
                        left: leftOffset,
                        width: this.fontSize / 2,
                        height: this.fontSize,
                        transform: 'translate(-50%, 0) rotate(' + rads + 'rad)'
                    },
                    idx: idx
                };
            });
        }
    }
});

var app = new Vue({
    el: "#root",
    data: {
        size: 100,
        digits: 2,
        selected: 2
    },
    methods: {}
});